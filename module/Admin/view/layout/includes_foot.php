<script src="<?php echo $this->basePath(); ?>/js/jquery.min.js"></script>
<script src="<?php echo $this->basePath(); ?>/js/nprogress.js"></script>
<script src="<?php echo $this->basePath(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo $this->basePath(); ?>/js/select/select2.full.js"></script> 

<!-- gauge js -->
<script src="<?php echo $this->basePath(); ?>/js/gauge/gauge.min.js" type="text/javascript"></script>
<script src="<?php echo $this->basePath(); ?>/js/gauge/gauge_demo.js" type="text/javascript"></script>
<!-- chart js -->
<script src="<?php echo $this->basePath(); ?>/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo $this->basePath(); ?>/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo $this->basePath(); ?>/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo $this->basePath(); ?>/js/icheck/icheck.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo $this->basePath(); ?>/js/moment.min.js" type="text/javascript"></script>
<script src="<?php echo $this->basePath(); ?>/js/datepicker/daterangepicker.js" type="text/javascript"></script>


<script src="<?php echo $this->basePath(); ?>/js/custom.js"></script>
<link href="<?php echo $this->basePath(); ?>/css/custom.css" rel="stylesheet">


<script type="text/javascript">


    $(document).ready(function () {
        // Smart Wizard     
        $('#wizard').smartWizard();

        function onFinishCallback() {
            $('#wizard').smartWizard('showMessage', 'Finish Clicked');
            //alert('Finish Clicked');
        }
    });

    $(document).ready(function () {
        // Smart Wizard 
        $('#wizard_verticle').smartWizard({
            transitionEffect: 'slide'
        });

    });
</script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>
