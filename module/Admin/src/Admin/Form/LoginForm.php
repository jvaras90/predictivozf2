<?php

namespace Admin\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class LoginForm extends Form {

    public function __construct($name = null) {

        parent::__construct('admin');

        $this->add(array(
            'name' => 'username',
            'options' => array(
                'label' => 'Usuario',
            ),
            'attributes'=>array(
                'type' => 'Text',
                'class'=> 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'options' => array(
                'label' => 'Contraseña',
            ),
            'attributes'=>array(
                'type' => 'password',
                'class'=> 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Iniciar Session',
                'id' => 'submitbutton',
                'class'=> 'btn btn-primary',
            ),
        ));
        
        
        //checkBox para aceptar terminos y condiciones
        $condiciones = new Element\Checkbox('rememberme');
        $condiciones->setLabel('Recordarme ');
        $condiciones->setAttributes(array(
            'class'=>'flat'
        ));
        $condiciones->setLabelAttributes(array(
            'class'=>'checkbox-inline'
        ));
        $this->add($condiciones);
        
        
//        $this->add(array(
//            'name' => 'rememberme',
//            'type' => 'Checkbox',
//            'options' => array(
//                'label' => 'No cerrar session',
//            ),
//            'attributes'=>array(
//                'type' => 'password',
//                'class'=> 'flat',
//            ),
//        ));
    }

}
