<?php

namespace Admin\Model;

use Zend\Authentication\Storage;

class MyAuthStorage extends Storage\Session {

    public function setRemenberMe($remenberMe = 0, $time = 1206900) {
        if ($remenberMe == 1) {
            $this->session->getManager()->remenberMe($time);
        }
    }

    public function forgetMe() {
        $this->session->getManager()->forgetMe();
    }

}
