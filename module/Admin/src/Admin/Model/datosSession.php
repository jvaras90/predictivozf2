<?php

namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class datosSession extends TableGateway {

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null) {
        return parent::__construct('personal', $adapter, $databaseSchema, $selectResultPrototype);
    }

    public function getDatosSession($user, $pass) {
        $rowset = $this->select(function(Select $select) use ($user, $pass) {
            $select->join('negocio', 'personal.idnegocio = negocio.idnegocio', array('negocio' => 'descripcion'))
                    ->join('tipo_nivel', 'personal.idtipo_nivel = tipo_nivel.idtipo_nivel', array('nivel' => 'descripcion'))
                    //->join('tabla2', 'tabla2.id = tabla1.id', array('aliasColum'=>'columnaTabla2')) 
                    ->where(array('usuario' => $user, 'clave' => md5($pass)))
            //->order('id_user ASC')
            ;
        });
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("No hay registros asociados al valor $user ");
        }
        return $row;
    }

}
