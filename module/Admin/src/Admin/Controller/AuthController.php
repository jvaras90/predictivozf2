<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\ViewModel;
use Admin\Form\LoginForm;
//session
use Zend\Session\Container;
use Admin\Model\datosSession;
use Zend\Db\Adapter\Adapter;

class AuthController extends AbstractActionController {

    protected $form;
    protected $storage;
    protected $authservice;
    public $dbAdapter;

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                    ->get('AuthService');
        }
        return $this->authservice;
    }

    public function getSessionStorage() {
        if (!$this->storage) {
            $this->storage = $this->getServiceLocator()
                    ->get('Admin\Model\MyAuthStorage');
        }
        return $this->storage;
    }

    public function getForm() {
        if (!$this->form) {
            $this->form = new LoginForm();
        }
        return $this->form;
    }

    public function loginAction() {
        //if already login, redirect to success page 
        if ($this->getAuthService()->hasIdentity()) {
            return $this->redirect()->toRoute('success');
        }

        $form = $this->getForm();
        // $form = new LoginForm('admin');
        $vm = new ViewModel(array(
            'form' => $form,
            'messages' => $this->flashmessenger()->getMessages()
        ));

        $this->layout('layout/login');

        return $vm;
    }

    public function authenticateAction() {
        $form = $this->getForm();
        $redirect = 'login';

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            $user = $request->getPost('username');
            $pass = $request->getPost('password');
            if ($user == "") {
                $this->flashmessenger()->addMessage("Ingrese su usuario porfavor.");
            } else {
                if ($form->isValid()) {
                    //check authentication...
                    $this->getAuthService()->getAdapter()
                            ->setIdentity($request->getPost('username'))
                            ->setCredential($request->getPost('password'));

                    $result = $this->getAuthService()->authenticate();

                    if ($result->isValid()) {
                        $container = new Container('user');
                        $this->dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter');
                        $u = new datosSession($this->dbAdapter);
                        $datos = $u->getDatosSession($user, $pass);
                        //var_dump($datos);exit();
                        $container->idpersona = $datos['idpersonal'];
                        $container->username = $datos['usuario'];
                        $nombres = $datos['nombres'] . " " . $datos['apaterno'];
                        $container->nombres = $nombres;
                        $container->email = $datos['mail'];
                        $container->id_negocio = $datos['idnegocio'];
                        $container->negocio = $datos['negocio'];
                        $container->dni = $datos['dni'];
                        $container->id_nivel = $datos['idtipo_nivel'];
                        $container->nivel = $datos['nivel'];
                        $container->foto = $datos['foto'];
                        $container->anexo = $datos['anexokb'];
                        $redirect = 'application';
                        //check if it has rememberMe :
                        if ($request->getPost('rememberme') == 1) {
                            $this->getSessionStorage()
                                    ->setRememberMe(1);
                            //set storage again 
                            $this->getAuthService()->setStorage($this->getSessionStorage());
                        }
                        $this->getAuthService()->getStorage()->write($request->getPost('username'));
                    } else {
                        $this->flashmessenger()->addMessage("Datos Incorrectos, intente nuevamente");
                    }
                }
            }
        }
        return $this->redirect()->toRoute($redirect);
    }

    public function logoutAction() {
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        $container = new Container('user');
        unset($container->username);
        unset($container->nombres);
        unset($container->email);
        unset($container->id_negocio);
        unset($container->negocio);
        unset($container->dni);
        unset($container->id_nivel);
        unset($container->nivel);
        unset($container->foto);
        unset($container->anexo);

        $this->flashmessenger()->addMessage("Desconectado del sistema");
        return $this->redirect()->toRoute('login');
    }

}
