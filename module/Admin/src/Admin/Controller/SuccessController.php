<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
//use Zend\Db\Sql\Sql;
//use Zend\Bd\ResultSet\ResultSet;
use Zend\Authentication\Storage;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

class SuccessController extends AbstractActionController {

    public $db;

    public function indexAction() {

        if (!$this->getServiceLocator()
                        ->get('AuthService')->hasIdentity()) {
            return $this->redirect()->toRoute('login');
        }
        $obj = new viewmodel();

        //$this->layout('layout/empty');
        return $obj;
    }

}
