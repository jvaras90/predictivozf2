/**=============================================================================
 *
 *	Filename:  function.ajax.js
 *	
 *	(c)Autor: Arkos Noem Arenom
 *	
 *	Description: Ajax para hacer las consultas
 *	
 *	Licence: GPL|LGPL
 *	
 *===========================================================================**/

$(document).ready(function () {
    var timeSlide = 1000;
    $('#login_username').focus();
    $('#timer').hide(0);
    $('#timer').css('display', 'none');
    $('#login_userbttn').click(function () {
        $('#timer').fadeIn(300);
        $('.box-info, .box-success, .box-alert, .box-error').slideUp(timeSlide);
        setTimeout(function () {
            if ($('#login_username').val() != "" && $('#login_userpass').val() != "") {

                $.ajax({
                    type: 'POST',
                    url: 'php/system/log.inout.ajax.php',
                    data: 'login_username=' + $('#login_username').val() + '&login_userpass=' + $('#login_userpass').val(),
                    success: function (msj) {
                        //alert(msj);
                        if (msj == 1) {
                            $('#alertBoxes').html('<div class="box-success"></div>');
                            $('.box-success').hide(0).html('Espera un momento&#133;');
                            $('.box-success').slideDown(timeSlide);
                            setTimeout(function () {
                                window.location.href = "index_2.php";
                            }, (timeSlide + 500));
                        } else if (msj == 2) { // es usuario
                            var user = $('#login_userpass').val();
                            $('#alertBoxes').html('<div class="box-success"></div>');
                            $('.box-success').hide(0).html('Espera un momento&#133;');
                            $('.box-success').slideDown(timeSlide);
                            setTimeout(function () {
                                window.location.href = "http://192.168.1.150/crm/crm.html?station=" + user;
                            }, (timeSlide + 500));
                        }
                        else {
                            $('#alertBoxes').html('<div class="box-error"></div>');
                            $('.box-error').hide(0).html('Lo sentimos, pero los datos son incorrectos: ' + msj);
                            $('.box-error').slideDown(timeSlide);
                        }
                        $('#timer').fadeOut(300);
                    },
                    error: function () {
                        $('#timer').fadeOut(300);
                        $('#alertBoxes').html('<div class="box-error"></div>');
                        $('.box-error').hide(0).html('Ha ocurrido un error durante la ejecuci&oacute;n');
                        $('.box-error').slideDown(timeSlide);
                    }
                });
            }
            else {
                $('#alertBoxes').html('<div class="box-error"></div>');
                $('.box-error').hide(0).html('Los campos estan vacios');
                $('.box-error').slideDown(timeSlide);
                $('#timer').fadeOut(300);
            }
        }, timeSlide);
        return false;
    });
    $('#sessionKiller').click(function () {
        $('#timer').fadeIn(300);
        $('#alertBoxes').html('<div class="box-success"></div>');
        $('.box-success').hide(0).html('Espera un momento&#133;');
        $('.box-success').slideDown(timeSlide);
        setTimeout(function () {
            window.location.href = "php/system/logout.php";
        }, 2500);
    });

});
