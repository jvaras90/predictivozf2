function AgregarTodos() {

    var datos = $('#listado_agentes_asignados').serialize();
    // var lista = document.getElementById("figura");
    //var numero = document.getElementById("numero");
    //var valorSeleccionado = lista.options[lista.selectedIndex].value;

    $.ajax({
        url: 'asignacionTodos.php',
        type: 'POST',
        async: true,
        data: datos,
        success: function (data) {
            $("#agregarTodos").html(data);
            document.getElementById("guardarAsignacionesValidator").className = "hidden";
            document.getElementById("guardarAsignacionesAfirmation").className = "btn btn-primary";
        }
    });
}

function agregarTablaTemporal() {
    /* Obtenemos el valor del campo */
    var valor = $('#buscarAgente').val();
    /* Si la longitud del valor es mayor a 2 caracteres.. */
    if (valor.length >= 3) {

        /* Cambiamos el estado.. */
        $('#estado').html('Cargando datos de servidor...');

        /* Hacemos la consulta ajax */
        var consulta = $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {opcion: valor},
            url: '../../php/system/codigoAsignado.php',
            cache: false
        });

        /* En caso de que se haya retornado bien.. */
        consulta.done(function (data) {
            if (!data) {
                $('#estado').html('Ha ocurrido un error: ' + data);
                return false;
            } else {
                $('#buscarAgente').val("");
                $('#example').append('<tr id="' + data[0] + '" class="impreso"><td><input type="hidden" name="agenteId' + data[0] + '" value="' + data[0] + '"/>' + data[0] + '</td><td>' + data[1] + '</td><td>' + data[2] + '</td><td><button type="button" class="btn btn-danger" onclick="eliminarAgente(' + data[0] + ');"><i class="fa fa-minus-square"></i> Quitar</button></td></tr>');
                $('#estado').html('Datos cargados..');
                document.getElementById("guardarAsignacionesValidator").className = "hidden";
                document.getElementById("guardarAsignacionesAfirmation").className = "btn btn-primary";
                return true;
            }
        });

        /* Si la consulta ha fallado.. */
        consulta.fail(function () {
            $('#estado').html('Ha habido un error contactando el servidor.');
            return false;
        });

    } else {
        /* Mostrar error */
        $('#estado').html('El nombre debe tener una longitud mayor a 2 caracteres...');
        return false;
    }
}
/* eliminar agentes de la tabla temporal de asignaciones */
function eliminarAgente(id)
{
    $('#' + id).remove();
}
/* Funcion para quitar agentes desde un negocio */
function eliminar(nombre, negocio, idnegocio, accion, idagente)
{
    if (confirm('Esta Apunto de Quitar a: ' + nombre + ' del negocio: ' + negocio + '\n¿Desea Continuar?'))
    {
        window.location.href = '../system/modificar.php?idneg=' + idnegocio + '&accion=' + accion + '&idagente=' + idagente;
    }
    else
    {
        alert('Usted ha Cancelado Esta Operacion.');
    }

}
/* Funcion para el boton cancelar en asignaciones.php*/
function cancelar(val)
{
    if(val==1){
       document.location.href = "asignacionCamp.php"; 
    }else if(val==2){
       document.location.href = "lista_campanas.php"; 
    }else if(val==3){
       document.location.href = "asignacionNeg.php"; 
    }
    
}
/* Funcion para validar el boton de asignar agentes*/
function nuevaAsignacion()
{
    if ($('.impreso').length == 0)
    {
        alert('Aún no hay equipos en la lista.');
    }
    else
    {
        formBaja = $('form').serialize();
        //window.open('../pdf/asignacionPdf.php?' + formBaja, '_blank');
        $('input[type=submit]').removeAttr('disabled');
        //$('#guardar').replaceWith()
    }
}
